# SW2 Documentation

You need `sphinx` and an extension called `sphinxcontrib.httpdomain` to compile rst files

## Install requirements
```
pip3 install -r requirements.txt
```

## Compile rst files
```
make html
```

## Preview documentation webpages
```
open build/html/index.html
```