Account
=======

POST /account/signup_token
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/signup_token

   A user provides email address, first name, last name,
   organization name, a security code
   to create an account and link to an organization.

   :jsonparam string account_type: account_type
   :jsonparam string email_address: the email address
   :jsonparam string first_name: the first name
   :jsonparam string last_name: the last name
   :jsonparam string organization_name: the organization name
   :jsonparam string security_code: the security code

   :statuscode 201: the user is created
   :statuscode 400: invalid email address
   :statuscode 400: invalid name
   :statuscode 400: invalid organization name
   :statuscode 401: the security code doesn't match the email address
   :statuscode 409: the email address already exists


POST /account/signup_file
^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/signup_file

   A user provides email address, first name, last name,
   organization name, files
   to create an account and link to an organization.

   :jsonparam string account_type: account_type
   :jsonparam string email_address: the email address
   :jsonparam string first_name: the first name
   :jsonparam string last_name: the last name
   :jsonparam string organization_name: the organization name
   :jsonparam file files: the supporting documents

   :statuscode 201: the user is created
   :statuscode 400: invalid email address
   :statuscode 400: invalid name
   :statuscode 400: invalid organization name
   :statuscode 401: the security code doesn't match the email address
   :statuscode 409: the email address already exists


GET /account/types
^^^^^^^^^^^^^^^^^^

.. http:get:: /account/types

   Get available account types for provider or buyer

   **Example response**

   .. sourcecode:: json

      {
        "types": [
           {
              "type_id": 0,
              "is_provider": true,
              "description": "audio_producer",
              "display": "Audio Producer / Studio"
           }
        ]
      }

   :query is_provider: whether return provider types

   :statuscode 200: ok

POST /account/login
^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/login

   **Example response**

   .. sourcecode:: json

      {
        "access_token": "235SDGgsr5gtu8"
      }

   :jsonparam string email_address: the email address
   :jsonparam string password: password

   :statuscode 200: ok
   :statuscode 400: validation error
   :statuscode 401: either not found or not matched

GET /account/activate
^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /account/activate

   :jsonparam string token: activation token

   :statuscode 200: valid activation token
   :statuscode 401: invalid activation token

POST /account/activate
^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/activate

   :jsonparam string password: initial password
   :jsonparam string token: activation token

   :statuscode 200: account is activated
   :statuscode 401: invalid activation token

GET /account
^^^^^^^^^^^^

.. http:get:: /account

   **Example response**

   .. sourcecode:: json

      {
        "user": {
          "first_name": "Elon",
          "last_name": "Musk",
          "email_address": "elon.musk@gmail.com",
          "phone_number": "1112223333",
          "address": {
            "line1": "123 Main St",
            "line2": "Suite 250",
            "city": "Los Angeles",
            "state": "California",
            "country": "US",
            "zip_code": "91234"
          }
        }
      }

   :statuscode 200: return user object
   :statuscode 401: invalid access token

PUT /account
^^^^^^^^^^^^

.. http:put:: /account

   **Example response**

   .. sourcecode:: json

      {
        "user": {
          "first_name": "Elon",
          "last_name": "Musk",
          "email_address": "elon.musk@gmail.com",
          "phone_number": "1112223333",
          "address": {
            "line1": "123 Main St",
            "line2": "Suite 250",
            "city": "Los Angeles",
            "state": "California",
            "country": "US",
            "zip_code": "91234"
          }
        }
      }

   :statuscode 200: return updated user object
   :statuscode 400: validation error
   :statuscode 401: invalid access token

POST /account/forget_password
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   :jsonparam string username: for now it's just email address

   :statuscode 200: reset password email sent

GET /account/reset_password
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /account/reset_password

   :jsonparam string token: activation token

   :statuscode 200: valid reset token
   :statuscode 401: invalid reset token

POST /account/reset_password
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/reset_password

   :jsonparam string password: updated password
   :jsonparam string token: reset token

   :statuscode 200: password is updated
   :statuscode 401: invalid reset token

POST /account/logout
^^^^^^^^^^^^^^^^^^^^

.. http:post:: /account/logout

   Immediately invalidate the access token on server side
