Organization Team
=================

GET /organization/users
^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /organization/users

   Get an organization's users (paginated)

   **Example response**

   .. sourcecode:: json

      {
        "users": {
          "items": [
            {
              "id": "0",
              "first_name": "Elon",
              "last_name": "Musk",
              "permissions": ["admin"]
            }, {
              "id": "1",
              "first_name": "Bill",
              "last_name": "Gates",
              "permissions": ["edit_payment", "edit_term"]
            }
          ],
          "offset": 0,
          "limit": 25,
          "total": 96,
          "previous": null,
          "next": "/organization/users?offset=25"
        }
      }

   :reqheader Authorization: Basic <access token>
   :statuscode 200: success
   :statuscode 401: access token problem

POST /organization/users
^^^^^^^^^^^^^^^^^^^^^^^^

.. http:post:: /organization/users

   Invite an email address to join an organization.

   :reqheader Authorization: Basic <access token>
   :jsonparam string email_address: the email address
   :jsonparam array permissions: a list of string permissions

   :statuscode 201: success
   :statuscode 401: access token problem
   :statuscode 403: the logged in user is not admin of the organization
   :statuscode 409: the email already exists

PUT /organization/users/:id
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:put:: /organization/users/:id

   Modify a user's permissions

   :reqheader Authorization: Basic <access token>
   :jsonparam string id: the id of the user
   :jsonparam array permissions: a list of string permissions

   :statuscode 200: success
   :statuscode 401: access token problem
   :statuscode 403: the logged in user is not admin of the organization
   :statuscode 404: the user is not in the same organization
